import UIKit

@IBDesignable

class RecordsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerScoores: UILabel!
    @IBOutlet weak var playerLastDatePlay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 3
        self.backgroundColor = UIColor(hue: 0.1, saturation: 0.0, brightness: 0.759, alpha: 1)
        self.playerName.textAlignment = .left
        self.playerScoores.textAlignment = .center
        self.playerLastDatePlay.textAlignment = .center
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func generateCell(player: Settings) {
        self.playerName.text = player.name
        guard let scoores = player.scoores else { return }
        self.playerScoores.text = String(scoores)
        self.playerLastDatePlay.text = player.lastPlayDate
    }
    
}
