import UIKit
import Foundation

class Config {
    
    var block = UIImageView()
    var car = UIImageView()
    var speed: CGFloat = 325
    
    func chooseBlock(player: Settings) {
        guard let blockModel = player.blockModel else { return }
        self.block.image = Manager.shared.getIconFor(blockModel)
    }
    
    func chooseCarModel(player: Settings) {
        guard let carModel = player.carModel else { return }
        self.car.image = Manager.shared.getIconFor(carModel)
    }
    
    func chooseDifficulty(player: Settings) {
        switch player.difficulty {
            case "Easy":
                self.speed = 600
            case "Normal":
                self.speed = 900
            case "Hard":
                self.speed = 1200
            default:
            break
        }
    }
    
    func loadStartSettings(player: Settings) {
        self.chooseBlock(player: player)
        self.chooseCarModel(player: player)
        self.chooseDifficulty(player: player)
    }
    
}
