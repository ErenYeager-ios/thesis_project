import Foundation
import  UIKit

class Settings: Codable {
    
    init(){}
    
    var name: String?
    var carModel: String?
    var blockModel: String?
    var difficulty: String?
    var lastPlayDate: String?
    var scoores: Int?
    var profileImage: UIImage?
    var isNewPlayer = true
    
    private enum CodingKeys: String, CodingKey {
        case name
        case carModel
        case blockModel
        case difficulty
        case lastPlayDate
        case scoores
        case profileImage
        case isNewPlayer
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        carModel = try container.decodeIfPresent(String.self, forKey: .carModel)
        blockModel = try container.decodeIfPresent(String.self, forKey: .blockModel)
        difficulty = try container.decodeIfPresent(String.self, forKey: .difficulty)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        lastPlayDate = try container.decodeIfPresent(String.self, forKey: .lastPlayDate)
        scoores = try container.decodeIfPresent(Int.self, forKey: .scoores)
        isNewPlayer = try container.decode(Bool.self, forKey: .isNewPlayer)
        if let data = try container.decodeIfPresent(Data.self, forKey: .profileImage) {
            profileImage = UIImage(data: data)
        } else {
            return
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.carModel, forKey: .carModel)
        try container.encode(self.blockModel, forKey: .blockModel)
        try container.encode(self.difficulty, forKey: .difficulty)
        try container.encode(self.lastPlayDate, forKey: .lastPlayDate)
        try container.encode(self.scoores, forKey: .scoores)
        try container.encode(self.isNewPlayer, forKey: .isNewPlayer)
        if let profileImage = profileImage {
            if let data = profileImage.jpegData(compressionQuality: 0.8) {
                try container.encode(data, forKey: .profileImage)
            }
        }
    }
    
    func loadDefaultsSettings() {
        self.name = "player"
        self.profileImage = nil
        self.carModel = Constants.carsIcons.first
        self.blockModel = Constants.blockIcons.first
        self.difficulty = Constants.carsIcons.first
        self.isNewPlayer = true
    }
    
}
