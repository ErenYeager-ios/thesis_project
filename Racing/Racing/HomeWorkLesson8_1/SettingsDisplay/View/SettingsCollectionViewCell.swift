import UIKit

class SettingsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var didSelectView: UIView!
    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.contentMode = .scaleAspectFit
        self.layer.cornerRadius = self.frame.width / 5
        self.didSelectView.layer.cornerRadius = self.didSelectView.frame.width / 5
        self.imageView.layer.cornerRadius = self.imageView.frame.width / 5
    }
    
}
