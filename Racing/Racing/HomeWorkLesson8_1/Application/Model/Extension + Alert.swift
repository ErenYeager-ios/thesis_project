import UIKit

enum AlertStyle: String {
    case ok
    case cansel
    case destructive
    case okAndCancel
}

extension UIViewController {
    func showAlert(title: String?, messaage: String?, style: AlertStyle, handler: ((UIAlertAction) -> Void)? = nil, textField: ((UITextField) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: messaage, preferredStyle: .alert)
        if textField != nil {
            alert.addTextField(configurationHandler: textField)
        }
        switch style {
            case .ok:
                let action = UIAlertAction(title: "OK", style: .default, handler: handler)
                alert.addAction(action)
            case .cansel:
                let action = UIAlertAction(title: "Cancel", style: .cancel, handler: handler)
                alert.addAction(action)
            case .destructive:
                let action = UIAlertAction(title: "Cancel", style: .destructive, handler: handler)
                alert.addAction(action)
            case .okAndCancel:
                let firstAction = UIAlertAction(title: "OK", style: .default, handler: handler)
                let secondAction = UIAlertAction(title: "Cancel", style: .cancel, handler: handler)
                alert.addAction(firstAction)
                alert.addAction(secondAction)
        }
        self.present(alert, animated: true, completion: nil)
    }
}
