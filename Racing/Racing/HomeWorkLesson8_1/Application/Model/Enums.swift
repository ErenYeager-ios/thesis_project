import UIKit

class Enums {
    
    enum PositionObstacles: String, CaseIterable {
        case firstPosition
        case secondPosition
        case thirdPosition
        case fourthPosition
    }
}
