import Foundation

class StorageManager{
    static let shared = StorageManager()
    private init(){}
    
    func savePlayersSettings(players: [Settings]) {
        UserDefaults.standard.set(encodable: players, forKey: Constants.key)
    }
    
    func loadPlayersSettings() -> [Settings]? {
        guard let playersSettings = UserDefaults.standard.value([Settings].self, forKey: Constants.key) else {return nil}
        return playersSettings
    }
    
}

