import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    var hue: CGFloat = 0.1
    var saturation: CGFloat = 0.0
    var brightness: CGFloat = 0.759
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {return}
        
        var actualBrightness = brightness
        if state == .highlighted {
            actualBrightness -= 0.2
        }
        
        let outerColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
        let shadowColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5)
        let outerMargin: CGFloat = 5.0
        let outerRect = rect.insetBy(dx: outerMargin, dy: outerMargin)
        let outerPath = createRoundedRectPath(rect: outerRect, radius: 15.0)
        if state != .highlighted {
            context.saveGState()
            context.setFillColor(outerColor.cgColor)
            context.setShadow(offset: CGSize(width: 0, height: 2.0), blur: 3.0, color:  shadowColor.cgColor)
            context.addPath(outerPath)
            context.fillPath()
            context.restoreGState()
        }
        let outerTop = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
        let outerBottom = UIColor(hue: hue, saturation: saturation, brightness: brightness * 0.8, alpha: 1.0)
        context.saveGState()
        context.addPath(outerPath)
        context.clip()
        drawGlossAndGradient(context: context, rect: outerRect, startColor: outerTop.cgColor, endColor: outerBottom.cgColor)
        context.restoreGState()
        let innerTop = UIColor(hue: hue, saturation: saturation, brightness: brightness * 0.9, alpha: 1.0)
        let innerBottom = UIColor(hue: hue, saturation: saturation, brightness: brightness * 0.7, alpha: 1.0)
        let innerMargin: CGFloat = 3.0
        let innerRect = outerRect.insetBy(dx: innerMargin, dy: innerMargin)
        let innerPath = createRoundedRectPath(rect: innerRect, radius: 10.0)
        context.saveGState()
        context.addPath(innerPath)
        context.clip()
        drawGlossAndGradient(context: context, rect: innerRect, startColor: innerTop.cgColor, endColor: innerBottom.cgColor)
        context.restoreGState()
    }
    
    func createRoundedRectPath(rect: CGRect, radius: CGFloat) -> CGMutablePath {
        let path = CGMutablePath()
        let midTopPoint = CGPoint(x: rect.midX, y: rect.minY)
        path.move(to: midTopPoint)
        let topRightPoint = CGPoint(x: rect.maxX, y: rect.minY)
        let bottomRightPoint = CGPoint(x: rect.maxX, y: rect.maxY)
        let bottomLeftPoint = CGPoint(x: rect.minX, y: rect.maxY)
        let topLeftPoint = CGPoint(x: rect.minX, y: rect.minY)
        path.addArc(tangent1End: topRightPoint, tangent2End: bottomRightPoint, radius: radius)
        path.addArc(tangent1End: bottomRightPoint, tangent2End: bottomLeftPoint, radius: radius)
        path.addArc(tangent1End: bottomLeftPoint, tangent2End: topLeftPoint, radius: radius)
        path.addArc(tangent1End: topLeftPoint, tangent2End: topRightPoint, radius: radius)
        path.closeSubpath()
        return path
    }
    
    func drawLinearGradient(context: CGContext, rect: CGRect, startColor: CGColor, endColor: CGColor) {
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocations: [CGFloat] = [0.0, 1.0]
        let colors: CFArray = [startColor, endColor] as CFArray
        let gradient = CGGradient(colorsSpace: colorSpace, colors: colors, locations: colorLocations)!
        let startPoint = CGPoint(x: rect.midX, y: rect.midY)
        let endPoint = CGPoint(x: rect.midX, y: rect.maxY)
        context.saveGState()
        context.addRect(rect)
        context.clip()
        context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
        context.restoreGState()
    }
    
    func drawGlossAndGradient(context: CGContext, rect: CGRect, startColor: CGColor, endColor: CGColor) {
        drawLinearGradient(context: context, rect: rect, startColor: startColor, endColor: endColor)
        let glossColor1 = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.35)
        let glossColor2 = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1)
        let topHalf = CGRect(origin: rect.origin, size: CGSize(width: rect.width, height: rect.height / 2))
        drawLinearGradient(context: context, rect: topHalf, startColor: glossColor1.cgColor, endColor: glossColor2.cgColor)
    }
    
}
